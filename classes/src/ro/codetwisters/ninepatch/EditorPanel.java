package ro.codetwisters.ninepatch;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.image.BufferedImage;

/**
 * User: codetwister
 * Date: 10/11/12
 * Time: 12:49 AM
 */
public class EditorPanel extends JPanel {

    private EditorControls editorControls;

    public EditorPanel(EditorControls.NinePatchSaveListener ninePatchSaveListener) {
        // init components and show
        setLayout(new BorderLayout());

        PreviewPanel previewPanel = new PreviewPanel();
        add(previewPanel, BorderLayout.CENTER);

        editorControls = new EditorControls(previewPanel, ninePatchSaveListener);
        add(editorControls, BorderLayout.WEST);
    }

    public void setImage(BufferedImage image) {
        editorControls.setImage(image);
    }

    public boolean isModified() {
        return editorControls.isModified();
    }

}
