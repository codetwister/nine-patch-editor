package ro.codetwisters.ninepatch;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: codetwister
 * Date: 10/14/12
 * Time: 2:37 AM
 */
public class NinePatchConverter {

    public static BufferedImage getResizedImageFromNinePatch(BufferedImage image, int width, int height) {
        // get patch regions
        ArrayList<PatchRange> horizontalPatchRanges = new ArrayList<PatchRange>();
        ArrayList<PatchRange> verticalPatchRanges = new ArrayList<PatchRange>();
        createPatchRanges(horizontalPatchRanges, image, true);
        createPatchRanges(verticalPatchRanges, image, false);
        // make sure width and height are bigger than original image
        if (width < image.getWidth()-2) width = image.getWidth()-2;
        if (height < image.getHeight()-2) height = image.getHeight()-2;

        calculateDestinationIndex(horizontalPatchRanges, image.getWidth() - 2, width);
        calculateDestinationIndex(verticalPatchRanges, image.getHeight() - 2, height);
        // create the image
        BufferedImage destinationImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        for (PatchRange verticalPatchRange : verticalPatchRanges) {
            for (PatchRange horizontalPatchRange : horizontalPatchRanges) {
                Graphics2D g = destinationImage.createGraphics();
                g.drawImage(image,
                        horizontalPatchRange.destinationStartIndex,
                        verticalPatchRange.destinationStartIndex,
                        horizontalPatchRange.destinationEndIndex+1,
                        verticalPatchRange.destinationEndIndex+1,
                        horizontalPatchRange.startIndex,
                        verticalPatchRange.startIndex,
                        horizontalPatchRange.endIndex+1,
                        verticalPatchRange.endIndex+1, null);
            }
        }
        return destinationImage;
    }

    public static BufferedImage checkImageIsNinePatch(BufferedImage image) {
        boolean ninePatch = true;
        for (int i = 1; i < image.getWidth()-1; i++) {
            int top = image.getRGB(i, 0);
            int bottom = image.getRGB(i, image.getHeight()-1);
            if ((top != Color.BLACK.getRGB() && top != 0) ||
                    (bottom != Color.BLACK.getRGB() && bottom != 0)) {
                ninePatch = false;
                break;
            }
        }
        for (int i = 1; i < image.getHeight()-1; i++) {
            int left = image.getRGB(0, i);
            int right = image.getRGB(image.getWidth()-1, i);
            if ((left != Color.BLACK.getRGB() && left != 0) ||
                    (right != Color.BLACK.getRGB() && right != 0)) {
                ninePatch = false;
                break;
            }
        }
        if (!ninePatch) {
            // make the image ninepatch
            BufferedImage npImage = new BufferedImage(image.getWidth()+2, image.getHeight()+2, BufferedImage.TYPE_INT_ARGB);
            Graphics g = npImage.createGraphics();
            g.drawImage(image, 1, 1, npImage.getWidth()-2, npImage.getHeight()-2, 0, 0, image.getWidth()-1, image.getHeight()-1, null);
            return npImage;
        } else {
            return image;
        }
    }

    private static void createPatchRanges(ArrayList<PatchRange> rangeList, BufferedImage image, boolean horizontal) {
        int size;
        if (horizontal) {
            size = image.getWidth()-1;
        } else {
            size = image.getHeight()-1;
        }
        int i = 1;
        while (i<size) {
            PatchRange latestPatch = (rangeList.size()>0)?rangeList.get(rangeList.size()-1):null;
            boolean currentStretch = (image.getRGB(horizontal?i:0, horizontal?0:i) == Color.BLACK.getRGB());
            if (latestPatch == null || latestPatch.stretch != currentStretch) {
                rangeList.add(new PatchRange(i, currentStretch));
                if (latestPatch != null) latestPatch.endIndex = i-1;
            }
            i++;
        }
        PatchRange latestPatch = (rangeList.size()>0)?rangeList.get(rangeList.size()-1):null;
        if (latestPatch != null) latestPatch.endIndex = size-1;
    }

    private static int getTotalStrechableSize(ArrayList<PatchRange> patchRanges) {
        int total = 0;
        for (PatchRange patchRange : patchRanges) {
            if (patchRange.stretch) total += patchRange.endIndex-patchRange.startIndex+1;
        }
        return total;
    }

    private static void calculateIndividualStrech(ArrayList<PatchRange> patchRanges, int totalStretch) {
        for (PatchRange patchRange : patchRanges) {
            if (patchRange.stretch) patchRange.stretchAmount = (double)(patchRange.endIndex-patchRange.startIndex+1)/(double)totalStretch;
        }
    }

    private static void calculateDestinationIndex(ArrayList<PatchRange> patchRanges, int originalSize, int destinationSize) {
        int totalStretchableSize = getTotalStrechableSize(patchRanges);
        int stretch = destinationSize - originalSize;
        calculateIndividualStrech(patchRanges, totalStretchableSize);
        PatchRange lastPatchRange = null;
        for (PatchRange patchRange : patchRanges) {
            if (lastPatchRange == null) {
                patchRange.destinationStartIndex = 0;
            } else {
                patchRange.destinationStartIndex = lastPatchRange.destinationEndIndex+1;
            }
            if (!patchRange.stretch) {
                patchRange.destinationEndIndex = patchRange.destinationStartIndex+patchRange.endIndex-patchRange.startIndex;
            } else {
                patchRange.destinationEndIndex = patchRange.endIndex-patchRange.startIndex+(int)Math.floor(patchRange.destinationStartIndex + (double) stretch * patchRange.stretchAmount);
            }
            lastPatchRange = patchRange;
        }
    }

    private static class PatchRange {
        int startIndex;
        int endIndex;
        boolean stretch;
        double stretchAmount = 1;
        int destinationStartIndex;
        int destinationEndIndex;

        private PatchRange(int startIndex, boolean stretch) {
            this.startIndex = startIndex;
            this.stretch = stretch;
        }

        @Override
        public String toString() {
            return " ("+(endIndex-startIndex+1)+" - "+(destinationEndIndex-destinationStartIndex+1)+") ";
        }
    }
}
