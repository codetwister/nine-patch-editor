package ro.codetwisters.ninepatch;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

/**
 * User: codetwister
 * Date: 10/14/12
 * Time: 4:09 PM
 */
public class EditorControls extends JPanel implements ChangeListener, NinePatchEditPanel.PixelSelectionListener, PreviewPanel.ResizeRequestListener {

    private static final int MIN_SCALE = 2;
    private static final int MAX_SCALE = 30;
    private static final int DEFAULT_SCALE = 5;

    private NinePatchEditPanel editPanel;
    private PreviewPanel previewPanel;
    private JSlider scaleSlider;
    private BufferedImage image;

    private int previewPatchWidth = 200;
    private int previewPatchHeight = 200;
    private boolean modified = false;

    public EditorControls(PreviewPanel previewPanel, final NinePatchSaveListener ninePatchSaveListener) {
        this.previewPanel = previewPanel;

        this.previewPanel.setListener(this);
        this.editPanel = new NinePatchEditPanel(this);
        editPanel.setScale(DEFAULT_SCALE);
        setBorder(BorderFactory.createTitledBorder("Editor"));
        setLayout(new BorderLayout());

        JPanel controlsPanel = new JPanel(new BorderLayout());
        JPanel scalePanel = new JPanel(new BorderLayout());
        JPanel scaleWrapper = new JPanel(new FlowLayout(FlowLayout.LEFT));
        scalePanel.add(new JLabel("Editor scale"), BorderLayout.WEST);
        scaleSlider = new JSlider(MIN_SCALE, MAX_SCALE, DEFAULT_SCALE);
        scaleSlider.setPreferredSize(new Dimension(150, 30));
        scaleSlider.addChangeListener(this);
        scalePanel.add(scaleSlider, BorderLayout.CENTER);

        JButton saveButton = new JButton("Save");
        JPanel buttonsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modified = false;
                ninePatchSaveListener.saveNinePatch(EditorControls.this.image);
            }
        });
        buttonsPanel.add(saveButton);
        scaleWrapper.add(scalePanel);
        controlsPanel.add(scaleWrapper, BorderLayout.SOUTH);
        controlsPanel.add(buttonsPanel, BorderLayout.NORTH);

        add(controlsPanel, BorderLayout.NORTH);
        add(editPanel, BorderLayout.CENTER);

        updatePreview();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource().equals(scaleSlider)) {
            editPanel.setScale(scaleSlider.getValue());
            this.revalidate();
            this.repaint();
        }
    }

    @Override
    public void pixelSelectionChanged(int position, Boolean[] pixelsSelected) {
        modified = true;
        int startX = 1;
        int startY = 1;
        int w = 1;
        int h = 1;
        int rgb[] = new int[pixelsSelected.length];
        for (int i = 0; i < pixelsSelected.length; i++) {
            Boolean aBoolean = pixelsSelected[i];
            rgb[i] = (aBoolean != null && aBoolean)?Color.BLACK.getRGB():0;
        }
        switch (position) {
            case NinePatchEditPanel.TOP:
                w = image.getWidth()-2;
                startY = 0;
                break;
            case NinePatchEditPanel.LEFT:
                h = image.getHeight()-2;
                startX = 0;
                break;
            case NinePatchEditPanel.BOTTOM:
                w = image.getWidth()-2;
                startY = image.getHeight()-1;
                break;
            case NinePatchEditPanel.RIGHT:
                h = image.getHeight()-2;
                startX = image.getWidth()-1;
                break;
        }
        image.setRGB(startX, startY, w, h, rgb, 0, 1);
        updatePreview();
    }

    @Override
    public void resizeRequested(int dx, int dy) {
        previewPatchWidth += dx;
        previewPatchHeight += dy;
        updatePreview();
    }

    private void updatePreview() {
        if (image == null) return;
        BufferedImage previewImage = NinePatchConverter.getResizedImageFromNinePatch(image, previewPatchWidth, previewPatchHeight);
        previewPatchWidth = previewImage.getWidth();
        previewPatchHeight = previewImage.getHeight();
        previewPanel.setImage(previewImage);
    }

    public void setImage(BufferedImage image) {
        this.image = image;
        // pass the image to the editPanel and to the preview panel...
        editPanel.setImage(image);
        updatePreview();
    }

    public boolean isModified() {
        return modified;
    }

    public interface NinePatchSaveListener {
        public void saveNinePatch(BufferedImage image);
    }
}
