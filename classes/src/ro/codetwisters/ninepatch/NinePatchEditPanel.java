package ro.codetwisters.ninepatch;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

/**
 * User: codetwister
 * Date: 10/13/12
 * Time: 4:09 AM
 */
public class NinePatchEditPanel extends JPanel implements MouseMotionListener, MouseListener {

    private static final int BAR_SIZE = 30;
    public static final int NONE = 0;
    public static final int TOP = 1;
    public static final int LEFT = 2;
    public static final int BOTTOM = 3;
    public static final int RIGHT = 4;

    private int scale = 5;
    private BufferedImage image;
    private int mousePositionX;
    private int mousePositionY;

    private Boolean[] selectedPixelsTop;
    private Boolean[] selectedPixelsLeft;
    private Boolean[] selectedPixelsRight;
    private Boolean[] selectedPixelsBottom;

    private PixelSelectionListener listener;

    private int selectedPixelBar = NONE;
    private int buttonPressed;

    public NinePatchEditPanel(PixelSelectionListener listener) {
        super();
        this.listener = listener;
        addMouseMotionListener(this);
        addMouseListener(this);
    }

    private void updateEditorForImage() {
        if (image == null) return;
        selectedPixelsTop = new Boolean[image.getWidth()-2];
        selectedPixelsBottom = new Boolean[image.getWidth()-2];
        selectedPixelsLeft = new Boolean[image.getHeight()-2];
        selectedPixelsRight = new Boolean[image.getHeight()-2];

        // initialize selected pixels
        for (int i = 1; i < image.getWidth() -1; i++) {
            selectedPixelsTop[i-1] = image.getRGB(i, 0) == Color.BLACK.getRGB();
            selectedPixelsBottom[i-1] = image.getRGB(i, image.getHeight()-1) == Color.BLACK.getRGB();
        }
        for (int i = 1; i < image.getHeight() -1; i++) {
            selectedPixelsLeft[i-1] = image.getRGB(0, i) == Color.BLACK.getRGB();
            selectedPixelsRight[i-1] = image.getRGB(image.getWidth()-1, i) == Color.BLACK.getRGB();
        }

        setSize(new Dimension(BAR_SIZE *2+image.getWidth(null)*scale, BAR_SIZE *2+image.getHeight(null)*scale));
        setPreferredSize(getSize());
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (image != null) {
            // paint bars
            g.setColor(Color.GREEN);
            // top
            g.fillRect(BAR_SIZE + scale, 0, image.getWidth(null)*scale - 2 * scale, BAR_SIZE);
            // left
            g.fillRect(0, BAR_SIZE +scale, BAR_SIZE, image.getHeight(null)*scale-2*scale);
            // right
            g.fillRect(BAR_SIZE +image.getWidth(null)*scale, BAR_SIZE +scale, BAR_SIZE, image.getHeight(null)*scale-2*scale);
            // bottom
            g.fillRect(BAR_SIZE +scale, BAR_SIZE +image.getHeight(null)*scale, image.getWidth(null)*scale-2*scale, BAR_SIZE);
            // paint image
            g.drawImage(image, BAR_SIZE, BAR_SIZE, image.getWidth(null)*scale, image.getHeight(null)*scale, null);

            //draw selected pixels
            g.setColor(Color.getHSBColor(0.3f, 0.5f, 0.3f));
            for (int i = 0; i < selectedPixelsTop.length; i++) {
                Boolean aBoolean = selectedPixelsTop[i];
                if (aBoolean != null && aBoolean) g.fillRect(BAR_SIZE + scale + i * scale, 0, scale, BAR_SIZE - 1);
            }
            for (int i = 0; i < selectedPixelsBottom.length; i++) {
                Boolean aBoolean = selectedPixelsBottom[i];
                if (aBoolean != null && aBoolean) g.fillRect(BAR_SIZE + scale + i * scale, BAR_SIZE + image.getHeight(null) * scale, scale, BAR_SIZE - 1);
            }
            for (int i = 0; i < selectedPixelsLeft.length; i++) {
                Boolean aBoolean = selectedPixelsLeft[i];
                if (aBoolean != null && aBoolean) g.fillRect(0, BAR_SIZE + scale + i * scale, BAR_SIZE - 1, scale);
            }
            for (int i = 0; i < selectedPixelsRight.length; i++) {
                Boolean aBoolean = selectedPixelsRight[i];
                if (aBoolean != null && aBoolean) g.fillRect(BAR_SIZE + image.getWidth(null) * scale, BAR_SIZE + scale + i * scale, BAR_SIZE - 1, scale);
            }

            //draw mouse position
            g.setColor(Color.BLUE);
            g.drawRect(BAR_SIZE +scale+mousePositionX*scale, 0, scale-1, BAR_SIZE -1);
            g.drawRect(BAR_SIZE +scale+mousePositionX*scale, BAR_SIZE +image.getHeight(null)*scale, scale-1, BAR_SIZE -1);
            g.drawRect(0, BAR_SIZE +scale+mousePositionY*scale, BAR_SIZE -1, scale-1);
            g.drawRect(BAR_SIZE +image.getWidth(null)*scale, BAR_SIZE +scale+mousePositionY*scale, BAR_SIZE -1, scale-1);
        } else {
            // draw a no image pattern
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        updateMousePosition(e);
        selectPixels();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // identify bounding box on rectangles...
        updateMousePosition(e);
    }

    private void updateMousePosition(MouseEvent e) {
        if (image == null) return;
        mousePositionX = (e.getX()- BAR_SIZE)/scale - 1;
        mousePositionY = (e.getY()- BAR_SIZE)/scale - 1;
        if (mousePositionX < 0) mousePositionX = 0;
        if (mousePositionX > image.getWidth(null)-3) mousePositionX = image.getWidth(null)-3;

        if (mousePositionY < 0) mousePositionY = 0;
        if (mousePositionY > image.getHeight(null)-3) mousePositionY = image.getHeight(null)-3;
        repaint();
        revalidate();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // Nothing to do here
    }

    @Override
    public void mousePressed(MouseEvent e) {
        buttonPressed = e.getButton();
        detectPressedPixelBar(e);
        updateMousePosition(e);
        selectPixels();
    }

    private void selectPixels() {
        if (image == null) return;
        Boolean[] pixelBar = null;
        int pixelIndex = -1;
        switch (selectedPixelBar) {
            case TOP:
                pixelBar = selectedPixelsTop;
                pixelIndex = mousePositionX;
                break;
            case BOTTOM:
                pixelBar = selectedPixelsBottom;
                pixelIndex = mousePositionX;
                break;
            case LEFT:
                pixelBar = selectedPixelsLeft;
                pixelIndex = mousePositionY;
                break;
            case RIGHT:
                pixelBar = selectedPixelsRight;
                pixelIndex = mousePositionY;
                break;
        }
        if (pixelBar != null && pixelIndex > -1 && pixelIndex < pixelBar.length) {
            pixelBar[pixelIndex] = buttonPressed == MouseEvent.BUTTON1;
            if (listener != null) listener.pixelSelectionChanged(selectedPixelBar, pixelBar);
            repaint();
            revalidate();
        }
    }

    private void detectPressedPixelBar(MouseEvent e) {
        if (image == null) return;
        int x = e.getX();
        int y = e.getY();
        if (x > BAR_SIZE +scale && x < BAR_SIZE +image.getWidth(null)*scale - scale &&
                y > 0 && y < BAR_SIZE) {
            selectedPixelBar = TOP;

        } else if (x > 0 && x < BAR_SIZE &&
                y > BAR_SIZE +scale && y < BAR_SIZE + image.getHeight(null)*scale-scale) {
            selectedPixelBar = LEFT;

        } else if (x > BAR_SIZE +scale && x < BAR_SIZE +image.getWidth(null)*scale - scale &&
                y > BAR_SIZE +image.getHeight(null)*scale && y < 2* BAR_SIZE +image.getHeight(null)*scale) {
            selectedPixelBar = BOTTOM;

        } else if (x > BAR_SIZE +image.getWidth(null)*scale && x < image.getWidth(null)*scale + 2 * BAR_SIZE &&
                y > BAR_SIZE +scale && y < BAR_SIZE + image.getHeight(null)*scale-scale) {
            selectedPixelBar = RIGHT;
        } else {
            selectedPixelBar = NONE;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // Nothing to do here
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // Nothing to do here
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Nothing to do here
    }

    public void setScale(int newScale) {
        scale = newScale;
        updateEditorForImage();
    }

    public void setImage(BufferedImage image) {
        this.image = image;
        updateEditorForImage();
    }

    public interface PixelSelectionListener {
        public void pixelSelectionChanged(int position, Boolean[] pixelsSelected);
    }
}
