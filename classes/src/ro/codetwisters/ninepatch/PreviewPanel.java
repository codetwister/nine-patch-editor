package ro.codetwisters.ninepatch;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * User: codetwister
 * Date: 10/11/12
 * Time: 12:54 AM
 */
public class PreviewPanel extends JPanel implements MouseListener, MouseMotionListener {

    private Image image;
    private Point lastLocation;
    private int imageX;
    private int imageY;
    private int buttonDown;

    private ResizeRequestListener listener;

    public PreviewPanel() {
        setBorder(BorderFactory.createTitledBorder("Preview"));
        addMouseListener(this);
        addMouseMotionListener(this);
        setPreferredSize(new Dimension(500, 500));
    }

    public void setListener(ResizeRequestListener listener) {
        this.listener = listener;
    }

    public void setImage(Image image) {
        this.image = image;
        revalidate();
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (image != null) g.drawImage(image, imageX, imageY, image.getWidth(null), image.getHeight(null), null);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // nothing to do here
    }

    @Override
    public void mousePressed(MouseEvent e) {
        lastLocation = e.getPoint();
        buttonDown = e.getButton();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        lastLocation = null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // nothing to do here
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // nothing to do here
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (buttonDown == MouseEvent.BUTTON1) {
            imageX += e.getX() - lastLocation.getX();
            imageY += e.getY() - lastLocation.getY();
            restrictImageToBounds();
            repaint();
            revalidate();
        } else if (listener != null) {
            listener.resizeRequested((int)(e.getX() - lastLocation.getX()), (int)(e.getY() - lastLocation.getY()));
        }
        lastLocation = e.getPoint();
    }

    private void restrictImageToBounds() {
        if (image == null) return;
        if ((imageX*2+image.getWidth(null))/2 < 0) {
            imageX = -image.getWidth(null)/2;
        }
        if ((imageY*2+image.getHeight(null))/2 < 0) {
            imageY = -image.getHeight(null)/2;
        }

        if ((imageX*2+image.getWidth(null))/2 > getWidth()) {
            imageX = getWidth()-image.getWidth(null)/2;
        }
        if ((imageY*2+image.getHeight(null))/2 > getHeight()) {
            imageY = getHeight()-image.getHeight(null)/2;
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    public interface ResizeRequestListener {
        public void resizeRequested(int dx, int dy);
    }
}
