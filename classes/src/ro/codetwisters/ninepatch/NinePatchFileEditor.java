package ro.codetwisters.ninepatch;

import com.intellij.codeHighlighting.BackgroundEditorHighlighter;
import com.intellij.ide.structureView.StructureViewBuilder;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorLocation;
import com.intellij.openapi.fileEditor.FileEditorState;
import com.intellij.openapi.fileEditor.FileEditorStateLevel;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * User: codetwister
 * Date: 10/21/12
 * Time: 12:53 AM
 */
public class NinePatchFileEditor implements FileEditor, EditorControls.NinePatchSaveListener {

    private VirtualFile virtualFile;
    private EditorPanel editorPanel;

    public NinePatchFileEditor(VirtualFile virtualFile) {
        this.virtualFile = virtualFile;
        editorPanel = new EditorPanel(this);
        try {
            editorPanel.setImage(NinePatchConverter.checkImageIsNinePatch(ImageIO.read(virtualFile.getInputStream())));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @NotNull
    @Override
    public JComponent getComponent() {
        return editorPanel;
    }

    @Override
    public JComponent getPreferredFocusedComponent() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @NotNull
    @Override
    public String getName() {
        return "9patch";  //To change body of implemented methods use File | Settings | File Templates.
    }

    @NotNull
    @Override
    public FileEditorState getState(@NotNull FileEditorStateLevel fileEditorStateLevel) {
        return FileEditorState.INSTANCE;
    }

    @Override
    public void setState(@NotNull FileEditorState fileEditorState) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isModified() {
        return editorPanel.isModified();
    }

    @Override
    public boolean isValid() {
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void selectNotify() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void deselectNotify() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void addPropertyChangeListener(@NotNull PropertyChangeListener propertyChangeListener) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void removePropertyChangeListener(@NotNull PropertyChangeListener propertyChangeListener) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public BackgroundEditorHighlighter getBackgroundHighlighter() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public FileEditorLocation getCurrentLocation() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public StructureViewBuilder getStructureViewBuilder() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void dispose() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public <T> T getUserData(@NotNull Key<T> tKey) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public <T> void putUserData(@NotNull Key<T> tKey, @Nullable T t) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void saveNinePatch(BufferedImage image) {
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(image, "png", os);
            virtualFile.setBinaryContent(os.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
