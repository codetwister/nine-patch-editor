package ro.codetwisters.ninepatch;

import com.intellij.openapi.components.ApplicationComponent;
import org.jetbrains.annotations.NotNull;

/**
 * User: codetwister
 * Date: 10/21/12
 * Time: 12:31 AM
 */
public class NinePatchApplicationComponent implements ApplicationComponent {
    public NinePatchApplicationComponent() {
    }

    public void initComponent() {
        // TODO: insert component initialization logic here
    }

    public void disposeComponent() {
        // TODO: insert component disposal logic here
    }

    @NotNull
    public String getComponentName() {
        return "NinePatchApplicationComponent";
    }
}
